jets:
  pt_btagJes:
    description: |
      pt of jet based on fixed MC15 calibration
    rank: 1
  absEta_btagJes:
    description: |
      absolute value of jet eta based on fixed MC15 calibration (used for network training)
    rank: 1
  eta_btagJes:
    description: |
      jet eta based on fixed MC15 calibration
    rank: 1
  jetPtRank:
    description: |
      rank of jet in pt-ordered list in event, rank 0 corresponds to highest-pt jet
    rank: 1
  pt:
    description: |
      calibrated pt of jet. We usually quantify performance in terms of this variable.
    rank: 2
  eta:
    description: |
      calibrated eta of jet. We usually quantify performance in terms of this variable.
    rank: 2
  mass:
    description: |
      mass of calibrated jet
    rank: 2
  energy:
    description: |
      energy of calibrated jet
    rank: 2
  bTagJVT:
    description: |
      jet vertex tagger score (used for pile-up rejection) of jet
    rank: 2

  IP2D_bc:
    description: |
      log-likelihood ratio (LLR) discriminant discriminating discriminating b-jets against c-jets,
      computed as the sum of per-track contributions using the template probability density functions (PDF)
      for the b- and c-flavour jet flavour hypotheses, respectively,
      assuming no correlation among the various tracks contributing to the sum.
    rank: 3
  IP2D_bu:
    description: |
      log-likelihood ratio (LLR) discriminant discriminating discriminating b-jets against light jets,
      computed as the sum of per-track contributions using the template probability density functions (PDF)
      for the b- and light-flavour jet flavour hypotheses, respectively,
      assuming no correlation among the various tracks contributing to the sum.
    rank: 3
  IP2D_cu:
    description: |
      log-likelihood ratio (LLR) discriminant discriminating discriminating c-jets against light jets,
      computed as the sum of per-track contributions using the template probability density functions (PDF)
      for the c- and light-flavour jet flavour hypotheses, respectively,
      assuming no correlation among the various tracks contributing to the sum.
    rank: 3
  IP2D_isDefaults:
    description: |
      default values are filled for this object
    rank: 3
  IP2D_nTrks:
    description: |
      number of tracks used in the computation of the log-likelihood ratio (LLR) discriminant
    rank: 3
  IP2D_pb:
    description: |
      template probability density function (PDF) for the b-jet flavour hypothesis
    rank: 3
  IP2D_pc:
    description: |
      template probability density function (PDF) for the c-jet flavour hypothesis
    rank: 3
  IP2D_pu:
    description: |
      template probability density function (PDF) for the light jet flavour hypothesis
    rank: 3

  IP3D_bc:
    description: |
      log-likelihood ratio (LLR) discriminant discriminating discriminating b-jets against light jets,
      computed as the sum of per-track contributions using the template probability density functions (PDF)
      for the b- and light-flavour jet flavour hypotheses, respectively,
      assuming no correlation among the various tracks contributing to the sum.
    rank: 4
  IP3D_bu:
    description: |
      log-likelihood ratio (LLR) discriminant discriminating discriminating b-jets against light jets,
      computed as the sum of per-track contributions using the template probability density functions (PDF)
      for the b- and light-flavour jet flavour hypotheses, respectively,
      assuming no correlation among the various tracks contributing to the sum.
    rank: 4
  IP3D_cu:
    description: |
      log-likelihood ratio (LLR) discriminant discriminating discriminating c-jets against light jets,
      computed as the sum of per-track contributions using the template probability density functions (PDF)
      for the c- and light-flavour jet flavour hypotheses, respectively,
      assuming no correlation among the various tracks contributing to the sum.
    rank: 4
  IP3D_isDefaults:
    description: |
      default values are filled for this object
    rank: 4
  IP3D_nTrks:
    description: |
      number of tracks used in the computation of the log-likelihood ratio (LLR) discriminant
    rank: 4
  IP3D_pb:
    description: |
      template probability density function (PDF) for the b-jet flavour hypothesis
    rank: 4
  IP3D_pc:
    description: |
      template probability density function (PDF) for the c-jet flavour hypothesis
    rank: 4
  IP3D_pu:
    description: |
      template probability density function (PDF) for the light-jet flavour hypothesis
    rank: 4

  SV1_L3d:
    description: |
      distance between the primary and the secondary vertex
    rank: 5
  SV1_Lxy:
    description: |
      transverse distance between the primary and secondary vertex
    rank: 5
  SV1_N2Tpair:
    description: |
      number of two-track vertex candidates
    rank: 5
  SV1_NGTinSvx:
    description: |
      number of tracks used in the secondary vertex
    rank: 5
  SV1_deltaR:
    description: |
      dR between the jet axis and the direction of the secondary vertex relative to the primary vertex
    rank: 5
  SV1_dstToMatLay:
    description: |
      distance from secondary vertex to the closest material layer
    rank: 5
  SV1_efracsvx:
    description: |
      energy fraction of the tracks associated with the secondary vertex: energy of vertex / energy of jet, considering charged tracks)
    rank: 5
  SV1_isDefaults:
    description: |
      default values are filled for this object
    rank: 5
  SV1_masssvx:
    description: |
      invariant mass of tracks at the secondary vertex assuming pion mass
    rank: 5
  SV1_significance3d:
    description: |
      distance between the primary and the secondary vertex divided by its uncertainty
    rank: 5
  SV1_correctSignificance3d:
    description: |
      corrected distance between the primary and the secondary vertex divided by its uncertainty, should be used instead of SV1_significance3d
    rank: 5


  JetFitter_N2Tpair:
    description: |
      number of two-track vertex candidates (prior to decay chain fit)
    rank: 6
  JetFitter_dRFlightDir:
    description: |
      unknown
    rank: 6
  JetFitter_deltaR:
    description: |
      unknown
    rank: 6
  JetFitter_deltaeta:
    description: |
      pseudorapidity distance deta between sum of all momenta at vertices
      and the fitted B-meson flight direction
    rank: 6
  JetFitter_deltaphi:
    description: |
      azimuthal distance dphi between sum of all momenta at vertices 
      and the fitted B-meson flight direction
    rank: 6
  JetFitter_energyFraction:
    description: |
      fraction of the charged jet energy in the secondary vertices
    rank: 6
  JetFitter_isDefaults:
    description: |
      default values are filled for this object
    rank: 6
  JetFitter_mass:
    description: |
      invariant mass of the tracks fitted to the vertices with at least two tracks
    rank: 6
  JetFitter_massUncorr:
    description: |
      unknown
    rank: 6
  JetFitter_nSingleTracks:
    description: |
      number of single track vertices
    rank: 6
  JetFitter_nTracksAtVtx:
    description: |
      number of tracks from multi-prong displaced vertices
    rank: 6
  JetFitter_nVTX:
    description: |
      number of vertices with more than one track
    rank: 6
  JetFitter_significance3d:
    description: |
      significance of the average distance between PV and displaced vertices,
      considering all multi-prong vertices or (if there are none) of all single-track vertices.
    rank: 6

  JetFitterSecondaryVertex_averageAllJetTrackRelativeEta: &jfsv
    description: |
      jet fitter secondary vertex algorithm properties (no further description)
    rank: 7
  JetFitterSecondaryVertex_averageTrackRelativeEta: *jfsv
  JetFitterSecondaryVertex_maximumAllJetTrackRelativeEta: *jfsv
  JetFitterSecondaryVertex_maximumTrackRelativeEta: *jfsv
  JetFitterSecondaryVertex_minimumAllJetTrackRelativeEta: *jfsv
  JetFitterSecondaryVertex_minimumTrackRelativeEta: *jfsv
  JetFitterSecondaryVertex_displacement2d:
    description: |
      transverse displacement of the secondary vertex from primary vertex (PV)
    rank: 7
  JetFitterSecondaryVertex_displacement3d:
    description: |
      distance of the secondary vertex from primary vertex (PV)
    rank: 7
  JetFitterSecondaryVertex_energy:
    description: |
      energy of charged tracks associated to secondary vertex
    rank: 7
  JetFitterSecondaryVertex_energyFraction:
    description: |
      fraction of charged jet energy in secondary vertex
    rank: 7
  JetFitterSecondaryVertex_isDefaults:
    description: |
      default values are filled for this object
    rank: 7
  JetFitterSecondaryVertex_mass:
    description: |
      invariant mass of tracks associated to secondary vertex
    rank: 7
  JetFitterSecondaryVertex_nTracks:
    description: |
      number of tracks associated to secondary vertex
    rank: 7

  rnnip_isDefaults:
    description: |
      default values are filled for this object
    rank: 8
  rnnip_pb:
    description: |
      score for b-jets obtained with a recurrent neural network based on impact parameter observables
    rank: 8
  rnnip_pc:
    description: |
      score for c-jets obtained with a recurrent neural network based on impact parameter observables
    rank: 8
  rnnip_pu:
    description: |
      score for light jets obtained with a recurrent neural network based on impact parameter observables
    rank: 8

  DL1_pb:
    description: |
      flavour tagging score of the DL1 tagger for b-jets.
      **High level tagger output!** Do not use for training!
    rank: 9
  DL1_pc:
    description: |
      flavour tagging score of the DL1 tagger for c-jets.
      **High level tagger output!** Do not use for training!
    rank: 9
  DL1_pu:
    description: |
      flavour tagging score of the DL1 tagger for light-flavour jets.
      **High level tagger output!** Do not use for training!
    rank: 9
  DL1r_pb:
    description: |
      flavour tagging score of the DL1r tagger for b-jets.
      **High level tagger output!** Do not use for training!
    rank: 9
  DL1r_pc:
    description: |
      flavour tagging score of the DL1r tagger for c-jets.
      **High level tagger output!** Do not use for training!
    rank: 9
  DL1r_pu:
    description: |
      flavour tagging score of the DL1r tagger for light-flavour jets.
      **High level tagger output!** Do not use for training!
    rank: 9
  actualInteractionsPerCrossing:
    description: |
      actual number of interactions per bunch crossing (mu)
    rank: 10
  averageInteractionsPerCrossing:
    description: |
      average number of interactions per bunch crossing (<mu>)
    rank: 10
  nPrimaryVertices:
    description: |
      average number of reconstructed primary vertices
    rank: 10
  eventNumber:
    description: |
      event number of the event the jet is associated with
    rank: 10
  mcEventWeight:
    description: |
      event weight of MC simulated event
      **Truth information!** Do not use for training!
    rank: 10
  beamSpotWeight:
    description: |
      event level beamspot weight, used to reweight to effectively change beamspot width
    rank: 10
  GhostBHadronsFinalCount:
    description: |
      number of associated b hadrons, based on whether they fall into
      the area clustered with the jet.
      **Truth information!** Do not use for training!
    rank: 11
  GhostBHadronsFinalPt:
    description: |
      pt of associated b hadrons.
      **Truth information!** Do not use for training!
    rank: 11
  GhostCHadronsFinalCount:
    description: |
      number of associated c hadrons.
      **Truth information!** Do not use for training!
    rank: 11
  HadronConeExclTruthLabelID:
    description: |
      jet label, using a geometric cone around the jet rather than the
      jet clustering algorithm.
      If a parton with a transverse momentum of more than 5 GeV is found within
      dR(q, jet) < 0.3 of the jet direction, the jet is labelled as a jet with the parton's flavour.
      The label should be one of: 0 (light
      jet), 4 (charm jet), 5 (bottom jet), or 15 (tau jet). Results
      should be similar to the ghost labeling. 
      **Truth information!** Do not use for training!
    rank: 11
  HadronConeExclExtendedTruthLabelID:
    description: |
      more detailed version of the HadronCone algorithm, with labels for
      double b-jets: 55 (double b-jets), 54 (bc jet).
      **Truth information!** Do not use for training!
    rank: 11
  HadronConeExclTruthLabelPt:
    description: |
      Transverse momentum of the labelling particle used for HadronConeExclTruthLabelID.
      **Truth information!** Do not use for training!
    rank: 11
  HadronConeExclTruthLabelLxy:
    description: |
      Decay radius of the labelling particle used for HadronConeExclTruthLabelID.
      **Truth information!** Do not use for training!
    rank: 11
  LeptonDecayLabel:
    description: |
      jet label for leptonic b hadron decays. The label is defined by the pdgid of the 
      leptons originating from the b hadron. The leptons from the b decay and c decay are counted.
      0: all hadronic b- and c- decays
      11 (13): electrons (muons) in either b or c decay
      11 + 13 = 24: electrons and muons in either b or c decay (not necessarily both in same decay)
      11 + 13 + 11(13) = 35 (37): both leptons in both decay, except no muons (electrons) in one of the decays
      11 + 11 + 13 + 13 = 48: both leptons in both decays.
      **Truth information!** Do not use for training!
    rank: 11
  nPromptLeptons:
    description: |
      Number of prompt leptons nearby the jet. **Truth information!** Do not use for training!
    rank: 11
  PartonTruthLabelID:
    description: |
      **Truth information!** Do not use for training!
    rank: 11

  n_tracks:
    description: |
      Number of tracks associated to the jet
    rank: 20
  n_tracks_loose:
    description: |
      Number of tracks associated to the jet (loose selection)
    rank: 20

tracks: &tracks
  valid:
    description: |
      valid flag, for more robust selection, true for any track that is defined
    rank: 1
  pt:
    description: |
      track transverse momentum
    rank: 100
  eta:
    description: |
      track pseudorapidity
    rank: 100
  theta:
    description: |
      track momentum polar angle
    rank: 100
  qOverP:
    description: |
      track charge divided by momentum magnitude
    rank: 100
  d0:
    description: |
      transverse impact parameter, distance of closest approach of the track to the primary vertex point in the r-phi projection
    rank: 100
  z0SinTheta:
    description: |
      longitudinal impact parameter projected onto the direction perpendicular to the track
    rank: 100
  z0RelativeToBeamspot:
    description: |
      longitudinal impact parameter projected onto the direction perpendicular to the track, relative to beamspot
    rank: 100

  d0Uncertainty:
    description: |
      uncertainty on track d0
    rank: 100
  z0SinThetaUncertainty:
    description: |
      uncertainty on track z0SinTheta
    rank: 100  
  z0RelativeToBeamspotUncertainty:
    description: |
      uncertainty on track z0RelativeToBeamspot
    rank: 100
  thetaUncertainty:
    description: |
      uncertainty on track theta
    rank: 100
  phiUncertainty:
    description: |
      uncertainty on track phi
    rank: 100
  qOverPUncertainty:
    description: |
      uncertainty on track qOverP
    rank: 100

  chiSquared:
    description: |
      chi2 of track particle fit
    rank: 101
  numberDoF:
    description: |
      number of degrees of freedom in track particle fit
    rank: 101

  numberOfInnermostPixelLayerHits:
    description: |
      number of hits in the IBL: could be 0, 1, or 2
    rank: 102
  numberOfInnermostPixelLayerSharedHits:
    description: |
      number of shared hits (contributing to the track fit and to another track) in the IBL
    rank: 102
  numberOfInnermostPixelLayerSplitHits:
    description: |
      number of split hits in the IBL
    rank: 102
  numberOfNextToInnermostPixelLayerHits:
    description: |
      number of hits in the next-to-innermost pixel layer: could be 0, 1, or 2
    rank: 102
  numberOfPixelHits:
    description: |
      combined number of hits in the pixel layers (including the IBL)
    rank: 102
  numberOfPixelHoles:
    description: |
      combined number of crossed active modules where no hit was found in the pixel layers (including the IBL)
    rank: 102
  numberOfPixelSharedHits:
    description: |
      number of shared hits (contributing to the track fit and to another track + not marked as split hit) in the pixel layers (including the IBL)
    rank: 102
  numberOfPixelSplitHits:
    description: |
      number of split hits in the pixel layers (including the IBL; split hit = hit is identified as being created by multiple charged particles during ambiguity solver stage at pattern recognition level)
    rank: 102
  numberOfSCTHits:
    description: |
      combined number of hits in the SCT layers (since 2 strip hits are required for a full SCT spacepoint, this number is divided by two in the track selection)
    rank: 102
  numberOfSCTHoles:
    description: |
      combined number of crossed active modules where no hit was found in the SCT layers
    rank: 102
  numberOfSCTSharedHits:
    description: |
      Number of shared hits (contributing to the track fit and to another track) in the SCT layers (since 2 strip hits are required for a full SCT spacepoint, this number is divided by two in the track selection)
    rank: 102

  expectInnermostPixelLayerHit:
    description: |
      Whether or not an IBL hit is expected based on dead sensor information
    rank: 102.5
  expectNextToInnermostPixelLayerHit:
    description: |
      Whether or not a B-layer hit is expected based on dead sensor information
    rank: 102.5
  radiusOfFirstHit:
    description: |
      Radius of the first hit on the track [mm]
    rank: 102.5

  ptfrac:
    description: |
      fraction of the jet pt carried by the track
    rank: 103
  deta:
    description: |
      pseudorapidity distance between track and jet
    rank: 103
  dphi:
    description: |
      azimuthal angle distance between track and jet
    rank: 103
  dr:
    description: |
      dR distance between track and jet
    rank: 103

  IP2D_signed_d0:
    description: |
      signed transverse impact parameter from IP2D algorithm
    rank: 104
  IP3D_signed_d0:
    description: |
      signed transverse impact parameter from IP3D algorithm
    rank: 104
  IP3D_signed_d0_significance:
    description: |
      signed transverse impact parameter significance (d0 / sigmal(d0)) from IP3D algorithm
    rank: 104
  IP3D_signed_z0:
    description: |
      signed longitudinal impact parameter from IP3D algorithm
    rank: 104
  IP3D_signed_z0_significance:
    description: |
      signed longitudinal impact parameter significance (z0 / sigmal(z0)) from IP3D algorithm
    rank: 104

  SV1VertexIndex:
    description: |
      index of SV1 vertex if this track was used in the construction of a SV1 vertex (defaults to -2)
    rank: 105
  JFVertexIndex:
    description: |
      index of JF vertex if this track was used in the construction of a JF vertex (defaults to -2)
    rank: 105
  AMVFWeightPV:
    description: |
      compatability of the track with the primary vertex if the track was used in the primary vertex fit 
      (0 if the track was not used in the primary vertex fit)
    rank: 105

  leptonID:
    description: |
      +\-11 if the track was used in the reconstruction of an electron. +\-13 for muon. If a track was not used
      in the reconstruction of a electron or muon, then 0.
    rank: 106

  truthVertexIndex:
    description: |
      truth vertex index of the track. 0 is reserved for the truth PV, any SVs are indexed arbitrarily with a int >0.
      Truth vertices within 0.1mm are merged.
    rank: 110
  truthOriginLabel:
    description: |
      truth origin of the track (PU=0, Fake=1, Primary=2, FromB=3, FromBC=4, FromC=5, FromTau=6, OtherSecondary=7). Defined
      [here](https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/PhysicsAnalysis/TrackingID/InDetTrackSystematicsTools/InDetTrackSystematicsTools/InDetTrackTruthOriginDefs.h#0137)
    rank: 110
  truthBarcode:
    description: |
      generator level truth barcode of the truth particle linked to this track (if one exists), used for linking 
      between different objects
    rank: 110
  truthParentBarcode:
    description: |
      barcode of the parent B/C hadron (if one exists) of the truth particle linked to this track (if one exists).
      Used for linking to particles in the `truth_hadrons` dataset.
    rank: 110


tracks_loose: &tracks_loose
  <<: *tracks


truth_fromBC:
  valid:
    description: |
      validity flag, for more robust selection, true for any truth particle that is defined
    rank: 1
  pt:
    description: |
      truth particle transverse momentum
    rank: 100
  eta:
    description: |
      truth particle pseudorapidity
    rank: 100
  phi:
    description: |
      truth particle azimuthal angle
    rank: 100


  dr:
    description: |
      truth particle dR(particle, jet)
    rank: 100

  Lxy:
    description: |
      truth particle decay radius
    rank: 100
  charge:
    description: |
      truth particle charge
    rank: 100

  flavour:
    description: |
      5 if the truth particle is a B hadron, 4 if the truth particle is a C hadron, -1 otherwise.
    rank: 110
  pdgId:
    description: |
      truth particle pdgId
    rank: 110
  barcode:
    description: |
      generator level barcode of the truth particle, used to link to objects in other datasets
    rank: 110
