#include "JetWriters/JetLinkedFlowWriter.h"
#include "JetWriters/JetElement.h"
#include "JetWriters/AssociationConfig.h"
#include "JetWriters/JetLinkWriter.h"

#include "xAODBase/IParticle.h"
#include "xAODPFlow/FlowElement.h"
#include "xAODJet/Jet.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODCaloEvent/CaloCluster.h"

namespace {

  namespace jw = jetwriters;

  // define functions to retrieve cluster and track objects from
  // particle flow objects
  auto getAssociatedFlowObjectConfig() {

    using Type = xAOD::FlowElement;
    using ElementType = JetElement<Type>;
    using PartLinks = std::vector<ElementLink<xAOD::IParticleContainer>>;

    // track getter
    auto track = [](const ElementType& el) -> const xAOD::TrackParticle* {
      if (!el.constituent) throw std::runtime_error("missing flow object");
      size_t n_charged = el.constituent->nChargedObjects();
      if (n_charged == 0) return nullptr;
      if (n_charged != 1) throw std::runtime_error(
        "n charged > 1, found " + std::to_string(n_charged));
      const auto* obj = el.constituent->chargedObject(0);
      if (!obj) throw std::runtime_error("charged object missing");
      const auto* track = dynamic_cast<const xAOD::TrackParticle*>(obj);
      if (!track) throw std::runtime_error("can't cast to track particle");
      return track;
    };

    // cluster getter
    auto cluster = [](const ElementType& el) -> const xAOD::CaloCluster* {
      if (!el.constituent) throw std::runtime_error("missing flow object");
      size_t n_clusters = el.constituent->nOtherObjects();
      if (n_clusters == 0) return nullptr;
      const xAOD::IParticle* obj = nullptr;
      if (n_clusters == 1) {
        obj = el.constituent->otherObject(0);
      } else {
        // if we have a few clusters take the one with the highest weight
        auto ops = el.constituent->otherObjectsAndWeights();
        obj = std::max_element(
          ops.begin(), ops.end(),
          [](const auto& x, const auto& y) { return x.second < y.second; }
          )->first;
      }
      if (!obj) { // very rare: handle slimmed negative energy clusters
        return nullptr;
      }
      const auto* cluster = dynamic_cast<const xAOD::CaloCluster*>(obj);
      if (!cluster) throw std::runtime_error("can't cast to calo cluster");
      return cluster;
    };

    // build the association tuple
    return std::tuple{
      jw::getAssociationConfig(
        [track] (const ElementType& e) -> const xAOD::TrackParticle* {
          return track(e);
        },"track"),
      jw::getAssociationConfig(
        [cluster](const ElementType& e) -> const xAOD::CaloCluster* {
          return cluster(e);
        }, "cluster")
    };
  }
}

JetLinkedFlowWriter::JetLinkedFlowWriter(
  H5::Group& output_file,
  const JetLinkWriterConfig& cfg):
  m_writer(nullptr)
{
  if (cfg.constituents.size != 0) {
    using T = xAOD::FlowElement;
    using I = xAOD::IParticleContainer;
    using A = decltype(getAssociatedFlowObjectConfig());
    m_writer.reset(
      new JetLinkWriter<T,I,A>(
        output_file, cfg, getAssociatedFlowObjectConfig()));
  }
}

JetLinkedFlowWriter::~JetLinkedFlowWriter() {
  flush();
}

JetLinkedFlowWriter::JetLinkedFlowWriter(JetLinkedFlowWriter&&) = default;

void JetLinkedFlowWriter::write(const xAOD::Jet& j)
{
  if (m_writer) m_writer->write(j);
}

void JetLinkedFlowWriter::flush()
{
  if (m_writer) m_writer->flush();
}
